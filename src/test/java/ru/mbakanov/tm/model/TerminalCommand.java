package ru.mbakanov.tm.model;

import ru.mbakanov.tm.constant.TerminalConst;

public class TerminalCommand {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.CMD_HELP, TerminalConst.ARG_HELP, "Show terminal`s commands."
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.CMD_ABOUT, TerminalConst.ARG_ABOUT, "Show developer info."
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.CMD_VERSION, TerminalConst.ARG_VERSION, "Show application version."
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.CMD_INFO, TerminalConst.ARG_INFO, "Show system hardware info."
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.CMD_EXIT, null ,"Close application."
    );

    private String name = "";

    private String arg = "";

    private String description = "";

    public TerminalCommand() {
    }

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) return;
        this.name = name;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        if (arg == null) return;
        this.arg = arg;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description == null) return;
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }
}
