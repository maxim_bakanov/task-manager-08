package ru.mbakanov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String ARG_HELP = "-h";

    String CMD_ABOUT = "about";

    String ARG_ABOUT = "-a";

    String CMD_VERSION = "version";

    String ARG_VERSION = "-v";

    String CMD_INFO = "info";

    String ARG_INFO = "-i";

    String CMD_EXIT = "exit";

    String VERSION_NUM = "1.0.8";

    String DEV_NAME = "Name: Maxim Bakanov";

    String DEV_EMAIL = "E-Mail: graonnirvald@gmail.com";

}
