package ru.mbakanov.tm;

import ru.mbakanov.tm.constant.TerminalConst;
import ru.mbakanov.tm.model.TerminalCommand;
import ru.mbakanov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        System.out.println("== Welcome to TASK MANAGER ==");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showUnexpected(arg);
                break;
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ARG_HELP:
                showHelp();
                break;
            case TerminalConst.ARG_ABOUT:
                showAbout();
                break;
            case TerminalConst.ARG_VERSION:
                showVersion();
                break;
            case TerminalConst.ARG_INFO:
                showInfo();
                break;
            default:
                showUnexpected(arg);
                break;
        }
    }

    private static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showHelp() {
        System.out.println(TerminalCommand.HELP.getName().toUpperCase());
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.INFO);
        System.out.println(TerminalCommand.EXIT);
    }

    private static void showVersion() {
        System.out.println(TerminalCommand.VERSION.getName().toUpperCase());
        System.out.println(TerminalConst.VERSION_NUM);
    }

    private static void showAbout() {
        System.out.println(TerminalCommand.ABOUT.getName().toUpperCase());
        System.out.println(TerminalConst.DEV_NAME);
        System.out.println(TerminalConst.DEV_EMAIL);
    }

    private static void showUnexpected(final String arg) {
        StringBuilder result = new StringBuilder();
        result.append("['").append(arg).append("' - unexpected command]");
        System.out.println(result);
    }

    private static void showInfo() {
        System.out.println(TerminalCommand.INFO.getName().toUpperCase());
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    private static void exit() {
        System.exit(0);
    }

}
